# Format: ABStree (ABS tree)

## The skinny

 * non-versioned
 * What most users know as "ABS"
 * The `abs` command syncs this to `/var/abs/` (by default)
 * Maintained by dbscripts

## Details

This is a flat directory tree, with the structure

 * `<repo>/<pkgbase>`
   The copy of `<pkgbase>` as it is available in `<repo>` for your
   architecture.
