# Format: PBS (Parabola Build System)

## The skinny

 * git-based (heavily)
 * Designed to have the UX of abstree/abslibre, but the capabilities
   of ABS

## Details

PBS is designed to look like ABS-tree, that is, a directory tree with
directories of the format:

    /<repo>/<pkgbase>

In addition to this, there is a `/.pbs-root` file to identify the root
directory of the PBS repository.

There are several branches; `master` is where development happens, and
there is a branch with the name of each architecture, which (should)
reflect what is currently available from the pacman repositories.

Where this really differs from ABS-tree is that each `<pkgbase>`
directory is actually a git submodule.  Despite being submodules, they
live in the same git repository; the default remote for them is the
git repository that they are submodules of.  That is, in
`.gitmodules`, the `url = ./` for each submodule.

Each package submodule there exists a file `.pbs-package` to identify
that directory as the root of a package.

In a package submodule there may be a `pbstrack` file with the format

    <git-remote-repository> packages/<pkgbase>

(or equivalently)

    <git-remote-repository> <git-remote-branch>

that identifies a package that it is downstream/forked from, and can
be used to get updates.
