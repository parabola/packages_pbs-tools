# Format: ABSLibre (ABS Libre)

## The skinny

 * git-based
 * Is the "abstree" format, but versioned with git
 * Used by Parabola
 * Canonically hosted at
   git://parabolagnulinux.org/abslibre/abslibre.git

## Details

This is the "abstree" format, but versioned in git.
