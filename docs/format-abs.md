# Format: ABS (Arch Build System)

## The skinny

 * SVN-based
 * Used by the Arch Linux developers.
 * Canonically hosted at
   svn://svn.archlinux.org/packages

## Details

ABS is an SVN-versioned flat directory tree.

 * `/<pkgbase>/trunk/`
   The "working copy" of `<pkgbase>`.
 * `/<pkgbase>/repos/<repo>-<arch>/`
   The version of <pkgbase> that is currently available on the
   repository `<repo>` (eg, `core`, `extra`) for `<arch>`
   architecture.

The developers themselves only make changes to `/<pkgbase>/trunk/`
directories, and `/<pkgbase>/trunk/` directories are maintained by
devtools/dbscripts.
