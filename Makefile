prefix=/usr/local

libreconfdir = /etc/libretools.d

# in a bind, thes can all be set to the same directory
pbs-bindir = $(prefix)/lib/pbs-core
git-bindir = $(prefix)/lib/git-core
bindir = $(prefix)/bin

pbs-progs := $(shell printf '%s\n' pbs-* | fgrep -v .)
git-progs := $(shell printf '%s\n' git-* | fgrep -v .)
bin-progs = pbs
libreconf := $(wildcard pbs-*.conf)

install-targets := \
	$(addprefix $(DESTDIR)$(pbs-bindir)/,$(pbs-progs)) \
	$(addprefix $(DESTDIR)$(git-bindir)/,$(git-progs)) \
	$(addprefix $(DESTDIR)$(bindir)/,$(bin-progs)) \
	$(addprefix $(DESTDIR)$(libreconfdir)/,$(libreconf))

# phony rules

all: PHONY $(pbs-progs) $(git-progs) $(bin-progs) $(libreconf)

clean: PHONY
	rm -f pbs

install: PHONY $(install-targets)

uninstall: PHONY
	rm -f -- $(install-targets)

# actual file rules

pbs: pbs.in
	sed 's|@pbs-bindir@|$(pbs-bindir)|g' < $< > $@
	chmod 755 $@

$(DESTDIR)$(pbs-bindir)/%: % | $(DESTDIR)$(pbs-bindir)
	cp '$*' '${@D}'

$(DESTDIR)$(git-bindir)/%: % | $(DESTDIR)$(git-bindir)
	cp '$*' '${@D}'

$(DESTDIR)$(bindir)/%: % | $(DESTDIR)$(bindir)
	cp '$*' '${@D}'

$(DESTDIR)$(libreconfdir)/%: % | $(DESTDIR)$(libreconfdir)
	install -m644 '$*' '${@D}'

$(addprefix $(DESTDIR),$(pbs-bindir) $(git-bindir) $(bindir) $(libreconfdir)):
	install -d '$@'

# tricks

.PHONY: PHONY
